'''
Write a program that reads an acrostic to identify the "hidden" word. Specifically, your program will receive a list 
of words (reprensenting an acrostic) and will need to return 
a string corresponding to the word that is spelled out by taking the first letter of each word in the acrostic.
return "".join( word[0] for word in acrostic )
'''

def read_out(acrostic):
    word = []
    for i in acrostic:
        word.append(i[:1])  
        str1 = ''.join(word)
    return str1
