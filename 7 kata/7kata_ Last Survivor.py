'''
You are given a string of letters and an array of numbers.
The numbers indicate positions of letters that must be removed, in order, starting from the beginning of the array.
After each removal the size of the string decreases (there is no empty space).
Return the only letter left.

Example:

let str = "zbk", arr = [0, 1]
    str = "bk", arr = [1]
    str = "b", arr = []
    return 'b'
Notes
The given string will never be empty.
The length of the array is always one less than the length of the string.
All numbers are valid.
There can be duplicate letters and numbers.
best practice
    l=[x for x in letters]
    [l.pop(x) for x in coords]
    return l[0]    

'''
def last_survivor(letters, coords):
    l = list(letters) 
    for i in coords:
        l.pop(i)
    c = str(l)
    return c[2:3]




print(last_survivor('abc', [1, 1]))