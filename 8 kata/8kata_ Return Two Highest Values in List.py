'''
In this kata, your job is to return the two distinct highest values in a list.
 If there're less than 2 unique values, return as many of them, as possible.

The result should also be ordered from highest to lowest.

Examples:

[4, 10, 10, 9]  =>  [10, 9]
[1, 1, 1]  =>  [1]
[]  =>  []
best practics = return sorted(set(arg1), reverse=True)[:2]
'''

def two_highest(arg1):
    if len(arg1) == 0:
        return arg1
    else:          
        a = max(arg1)    
        arg1.remove(a)
        res = []
        res.append(a)
        if len(arg1)>0:
            c = max(arg1)
            while a==c:
                arg1.remove(c)
                c = max(arg1)                 
            b = max(arg1)
            res.append(b)
    return res
l = [20, 20, 20, 10]
print(two_highest(l))