'''
С использованием модуля Random сформировать одномерный массив, 
состоящий из n вещественных элементов в котором элементы случайным 
образом принимают положительный или отрицательный знак и значение 
от - 10 до 10. 
Для заданного числа y, такого, что amin < y < amax, вычислить:
1. Произведение элементов массива, значения модуля которых больше y.
2. Сумму модулей остальных элементов.
'''
import random 
from functools import reduce 

n = int(input('please insert count elements '))
l = [(random.randint(-10,10)) for i in range(0, n)]
print(l)
amn = 1
amx = len(l)
print('enter a number from', amn, ' to', amx)
n = int(input())
multiplication = 1
summa = 0
for i in l:
    if abs(i)>n:
        multiplication = multiplication*i
    else:
        summa = summa + i
print('The sum of elements is less than y', summa)
print('The sum of elements is greater than y ',multiplication)